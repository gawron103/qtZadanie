#ifndef GRAYSCALE_H
#define GRAYSCALE_H

#include "transformation.h"

/**
 * @brief The Grayscale class
 * The task of this class is to transform given image to grayscale
 */
class Grayscale : public Transformation
{
public:
    /**
     * @brief Grayscale -> default constructor
     */
    Grayscale();
    ~Grayscale();

    /**
     * @brief transform -> implemented pure virtual method from class Transformation
     * @param inputImage -> given image
     * @return struct of modified image
     */
    Image transform(const Image& inputImage);
};

#endif // GRAYSCALE_H
