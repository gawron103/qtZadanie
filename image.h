#ifndef IMAGE_H
#define IMAGE_H

#include <vector>
#include <iostream>

/**
 * @brief The Size struct
 * Struct representing image size
 */
struct Size
{
    /**
     * @brief m_width -> width of image
     */
    int m_width;
    /**
     * @brief m_height -> height of image
     */
    int m_height;

    /**
     * @brief Size -> default constructor
     */
    Size() {}
    /**
     * @brief Size -> constructor
     * @param width -> width of image
     * @param height -> height of image
     */
    Size(int width, int height) : m_width(width), m_height(height) {}
};

/**
 * @brief The ImageType enum -> type of image */
enum class ImageType
{
    Rgba,
    Grayscale
};

/**
 * @brief The Image struct -> struct representing image
 */
struct Image
{
    /**
     * @brief size -> struct Size
     */
    Size size;
    /**
     * @brief type -> type of image
     */
    ImageType type;
    /**
     * @brief data -> image data
     */
    std::vector<uint8_t> data;

    /**
     * @brief pixelCount -> method that returns number of pixels
     * @return number of pixels
     */
    int pixelCount() const
    {
        return size.m_height * size.m_width;
    }

    void setPixel(int row, int col, uint8_t value)
    {
        data[row * size.m_width + col] = value;
    }

    void fill(uint8_t value)
    {
        std::fill(data.begin(), data.end(), value);
    }

    /**
     * @brief Image -> default constructor
     */
    Image() {}
    /**
     * @brief Image -> constructor
     * @param _size -> size of image
     * @param _type -> type of image
     */
    Image(Size _size, ImageType _type) : size(_size), type(_type) {}
    ~Image() {}
};

/**
 * @brief The TransformOperation enum -> possible transform operations
 */
enum class TransformOperation
{
    Nothing,
    Histogram,
    Gray,
    Median,
    Average
};

#endif // IMAGE_H


