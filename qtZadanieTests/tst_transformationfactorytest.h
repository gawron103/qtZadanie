#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <../transformationfactory.h>
#include <../transformation.cpp>
#include <../image.h>

#include <typeinfo>

using namespace testing;

//PODANIE ZLEJ OPERACJI
TEST(TransformationFactoryTest, WrongParameterTest)
{
    TransformationFactory factory;

    std::string transformationName = "wrong";

    ASSERT_EQ(nullptr, factory.createTransformation(transformationName));

}

/* ---------- TESTY KONSTRUKTORA ---------- */
TEST(TransformationFactoryTest, ConstructorGrayTest)
{
    TransformationFactory factory;

    ASSERT_EQ(TransformOperation::Gray, factory.m_possibleInputOperations["gray"]);
}

TEST(TransformationFactoryTest, ConstructorMedianTest)
{
    TransformationFactory factory;

    ASSERT_EQ(TransformOperation::Median, factory.m_possibleInputOperations["median"]);
}

TEST(TransformationFactoryTest, ConstructorAverageTest)
{
    TransformationFactory factory;

    ASSERT_EQ(TransformOperation::Average, factory.m_possibleInputOperations["average"]);
}
/* ---------- KONIEC TESTOW KONSTRUKTORA ---------- */
