#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <../grayscale.cpp>
#include <../transformation.cpp>
#include <../image.h>

/**
 * ASSERT ---> fails fast, aborting the current function
 * EXPECT ---> continues after failure
 */

using namespace testing;

TEST(GrayscaleTest, OutputSizeRightTest)
{
    Grayscale gray;

    Image testImage(Size(300, 300), ImageType::Rgba);
    testImage.data.resize((300 * 300) * 4);
    std::fill(testImage.data.begin(), testImage.data.end(), 0);

    EXPECT_EQ(90000, gray.transform(testImage).data.size());
}
