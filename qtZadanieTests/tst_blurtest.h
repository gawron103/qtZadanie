#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <iostream>

#include <../transformation.h>
#include <../image.h>
#include <../average.h>

using namespace testing;

TEST(BlurTest, TransformTest)
{
    Image *blurTestImage = new Image(Size(25, 25), ImageType::Rgba);

    blurTestImage->data.resize((blurTestImage->size.m_height * blurTestImage->size.m_width) * 4);
    std::fill(blurTestImage->data.begin(), blurTestImage->data.end(), 145);

    Image *referenceImage = new Image(Size(25, 25), ImageType::Grayscale);
    referenceImage->data.resize((referenceImage->size.m_height * referenceImage->size.m_width));
    std::fill(referenceImage->data.begin(), referenceImage->data.end(), 145);

    Average *average = new Average(2);
    Image outputImage = average->transform(*blurTestImage);

    ASSERT_EQ(referenceImage->data.size(), outputImage.data.size());
    ASSERT_EQ(referenceImage->type, outputImage.type);
    ASSERT_EQ(referenceImage->size.m_height, outputImage.size.m_height);
    ASSERT_EQ(referenceImage->size.m_width, outputImage.size.m_width);

    delete blurTestImage;
    delete referenceImage;
    delete average;
}
