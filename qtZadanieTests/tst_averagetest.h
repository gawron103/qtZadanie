#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <../average.h>
#include <../blur.h>
#include <../transformation.cpp>
#include <../image.h>

using namespace testing;

TEST(AverageTest, ModifyDataTest)
{
    std::vector<uint8_t> testMask(9, 2);

    Average *average = new Average(1);

    ASSERT_EQ(2, average->modifyData(testMask));

    delete average;
}
