#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <iostream>

#include <../app.h>
#include <../app.cpp>
#include <../transformation.h>
#include <../transformation.cpp>
#include <../transformationfactory.cpp>
#include <../blur.cpp>
#include <../median.cpp>
#include <../average.cpp>
#include <../histogram.cpp>
#include <../image.h>

using namespace testing;

const int testParametersCount = 4;

char **testArgv = new char *[testParametersCount];

Image *testImage = new Image();

void initTestObjects(char **testArgv, Image *testImage)
{
    testArgv[0] = (char*)"pleple";
    testArgv[1] = (char*)"newTestPic.jpg";
    testArgv[2] = (char*)"gray";
    testArgv[3] = (char*)"out.jpg";

    testImage->size.m_height = 25;
    testImage->size.m_width = 25;

    testImage->type = ImageType::Rgba;
}

/* ---------- TESTY WCZYTYWANIA PLIKU ---------- */
//KIEDY WSZYSTKO JEST OK
TEST(AppTest, PositiveLoadFileTest)
{
    initTestObjects(testArgv, testImage);

    App *app = new App(testParametersCount, testArgv);

    ASSERT_EQ(0, app->loadFile());

    delete app;
}

//PLIK NIE ISTNIEJE, POWINIEN WEJSC W 2 IFA / PLIK NIE ISTNIEJE
TEST(AppTest, FileDoesntExistTest)
{
    initTestObjects(testArgv, testImage);

    testArgv[1] = (char*)"ExamplePic.jpg";

    App *app = new App(testParametersCount, testArgv);

    ASSERT_EQ(-1, app->loadFile());

    delete app;
}

//COS JEST NIE TAK Z PLIKIEM, POWINIEN WEJSC W 3 IFA / COS JEST NIE TAK Z PLIKIEM
TEST(AppTest, FileIsNullTest)
{
    initTestObjects(testArgv, testImage);

    testArgv[1] = (char*)"testPic.jpg";

    App *app = new App(testParametersCount, testArgv);

    ASSERT_EQ(-1, app->loadFile());

    delete app;
}

//PODANY PLIK JEST W SKALI SZAROSCI, POWINIEN WEJSC W 4 IFA
TEST(AppTest, InputFileIsGrayscaleTest)
{
    initTestObjects(testArgv, testImage);

    testArgv[1] = (char*)"inputGrayscale.jpg";

    App *app = new App(testParametersCount, testArgv);

    ASSERT_EQ(0, app->loadFile());

    delete app;
}

/* ---------- KONIEC TESTOW WCZYTYWANIA PLIKU ---------- */

/* ---------- TEST KONSTRUKTORA ---------- */
//DODAC SPRAWDZANIE CZY MAPY SA WYPELNIONE PRAWIDLOWO
TEST(AppTest, GoodConstructorTest)
{
    initTestObjects(testArgv, testImage);

    App *app = new App(testParametersCount, testArgv);

    ASSERT_EQ(4, app->m_parametersCounter);
    ASSERT_EQ(0, app->m_programStatus);

    delete app;
}

//jesli ilosc podanych danych jest mniejsza niz 3
TEST(AppTest, BadConstructorTest)
{
    initTestObjects(testArgv, testImage);

    App *app = new App(2, testArgv);

    ASSERT_EQ(-1, app->m_programStatus);

    delete app;
}
/* ---------- KONIEC TESTOW KONSTRUKTORA ---------- */

/* ---------- TEST ZAPISU ---------- */
TEST(AppTest, SaveSuccessTransformedTest)
{
    initTestObjects(testArgv, testImage);

    testImage->data.resize(testImage->size.m_height * testImage->size.m_width);

    std::fill(testImage->data.begin(), testImage->data.end(), 145);

    testArgv[3] = (char*)"saveTest.jpg";

    App *app = new App(testParametersCount, testArgv);

    ASSERT_EQ(0, app->saveTransformedFile(*testImage));

    delete app;
}

TEST(AppTest, SaveFailureTransformedTest)
{
    initTestObjects(testArgv, testImage);

    testImage->data.clear();

    testArgv[3] = (char*)"saveTest.jpg";

    App *app = new App(testParametersCount, testArgv);

    ASSERT_EQ(-1, app->saveTransformedFile(*testImage));

    delete app;
}

/* ---------- KONIEC TESTOW ZAPISU ---------- */

/* ---------- TEST ILOSCI DODATKOWYCH DANYCH MODYFIKACJI ---------- */
TEST(AppTest, checkWrongInputParametersCountTest)
{
    TransformOperation operation = TransformOperation::Gray;

    App *app = new App(3, testArgv);
    ASSERT_EQ(0, app->m_programStatus);

    app->checkInputParametersCount(operation);

    ASSERT_EQ(-1, app->m_programStatus);

    delete app;
}

TEST(AppTest, checkGoodInputParametersCountTest)
{
    TransformOperation operation = TransformOperation::Gray;

    App *app = new App(testParametersCount, testArgv);
    ASSERT_EQ(0, app->m_programStatus);

    app->checkInputParametersCount(operation);

    ASSERT_EQ(0, app->m_programStatus);

    delete app;
}
/* ---------- KONIEC TESTOW ILOSCI DODATKOWYCH DANYCH MODYFIKACJI ---------- */

/* ---------- TEST TWORZENIA MODYFIKACJI ---------- */
//GRAYSCALE
TEST(AppTest, makePositiveGrayscaleModificationTest)
{
    initTestObjects(testArgv, testImage);

    testImage->data.resize(testImage->size.m_height * testImage->size.m_width);
    std::fill(testImage->data.begin(), testImage->data.end(), 145);

    App *app = new App(testParametersCount, testArgv);
    app->makeModification(*testImage);

    ASSERT_EQ(0, app->m_programStatus);

    delete app;
}

//AVERAGE
TEST(AppTest, makePositiveAverageModificationTest)
{
    int testNewParametersCount = 5;
    char **testData = new char *[testNewParametersCount];

    testData[0] = (char*)"example";
    testData[1] = (char*)"newTestPic.jpg";
    testData[2] = (char*)"average";
    testData[3] = (char*)"2";
    testData[4] = (char*)"averageOut.jpg";

    Image *testNewImage = new Image(Size(25, 25), ImageType::Rgba);

    testNewImage->data.resize((testNewImage->size.m_height * testNewImage->size.m_width) * 4);
    std::fill(testNewImage->data.begin(), testNewImage->data.end(), 145);

    App *app = new App(testNewParametersCount, testData);

    ASSERT_EQ(0, app->makeModification(*testNewImage));

    delete testNewImage;
    delete[] testData;
    delete app;
}

//MEDIAN
TEST(AppTest, makePositiveMedianModificationTest)
{
    int testNewParametersCount = 5;
    char **testData = new char *[testNewParametersCount];

    testData[0] = (char*)"example";
    testData[1] = (char*)"newTestPic.jpg";
    testData[2] = (char*)"median";
    testData[3] = (char*)"2";
    testData[4] = (char*)"medianOut.jpg";

    Image *testNewImage = new Image(Size(25, 25), ImageType::Rgba);

    testNewImage->data.resize((testNewImage->size.m_height * testNewImage->size.m_width) * 4);
    std::fill(testNewImage->data.begin(), testNewImage->data.end(), 145);

    App *app = new App(testNewParametersCount, testData);

    ASSERT_EQ(0, app->makeModification(*testNewImage));

    delete testNewImage;
    delete[] testData;
    delete app;
}

//HISTOGRAM
TEST(AppTest, makePositiveHistogramModificationTest)
{
    int testNewParametersCount = 3;
    char **testData = new char *[testNewParametersCount];

    testData[0] = (char*)"example";
    testData[1] = (char*)"newTestPic.jpg";
    testData[2] = (char*)"histogram";

    Image *testNewImage = new Image(Size(25, 25), ImageType::Rgba);

    testNewImage->data.resize((testNewImage->size.m_height * testNewImage->size.m_width) * 4);
    std::fill(testNewImage->data.begin(), testNewImage->data.end(), 145);

    App *app = new App(testNewParametersCount, testData);

    ASSERT_EQ(0, app->makeModification(*testNewImage));

    delete testNewImage;
    delete[] testData;
    delete app;
}

//BLEDNY TEST
TEST(AppTest, makeNegativeModificationTest)
{
    initTestObjects(testArgv, testImage);

    testImage->data.resize(testImage->size.m_height * testImage->size.m_width);
    std::fill(testImage->data.begin(), testImage->data.end(), 145);

    App *app = new App(testParametersCount, testArgv);

    app->m_programStatus = -1;

    ASSERT_EQ(-1, app->makeModification(*testImage));

    delete app;
    delete testImage;
    delete []testArgv;
}

/* ---------- KONIEC TESTOW TWORZENIA MODYFIKACJI---------- */
