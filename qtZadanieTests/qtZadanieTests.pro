include(gtest_dependency.pri)

QT += core
QT += gui

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += thread

HEADERS +=     tst_grayscaletest.h \
    tst_apptest.h \
    tst_averagetest.h \
    tst_mediantest.h \
    tst_transformationfactorytest.h \
    tst_blurtest.h \
    tst_histogramtest.h

SOURCES +=     main.cpp

QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
QMAKE_LFLAGS += -g -Wall -fprofile-arcs -ftest-coverage  -O0

LIBS += \
    -lgcov
