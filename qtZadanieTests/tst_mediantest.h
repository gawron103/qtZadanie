#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <../median.h>
#include <../blur.h>
#include <../transformation.cpp>
#include <../image.h>

using namespace testing;

TEST(MedianTest, ModifyDataTest)
{
    std::vector<uint8_t> testMask(9, 1);

    Median *median = new Median(1);

    ASSERT_EQ(1, median->modifyData(testMask));

    delete median;
}
