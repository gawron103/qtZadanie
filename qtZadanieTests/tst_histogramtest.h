#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <../histogram.h>
#include <../image.h>

using namespace testing;

TEST(HistogramTest, ConstructorTest)
{
    Histogram histogram;

    ASSERT_EQ(-1, histogram.m_programStatus);
}

TEST(HistogramTest, createPositiveRGBHistogramTest)
{
    Image testImage(Size(25, 25), ImageType::Rgba);

    testImage.data.resize((testImage.size.m_width * testImage.size.m_height) * 4);
    std::fill(testImage.data.begin(), testImage.data.end(), 144);

    const int testPixelComponent = 1;

    Histogram histogram;
    auto testHistogramVector = histogram.createRGBHistogram(testImage, testPixelComponent);

    ASSERT_EQ(255, testHistogramVector.size());
    ASSERT_EQ(testImage.size.m_height * testImage.size.m_width, testHistogramVector[144]);
    ASSERT_EQ(0, testHistogramVector[23]);
}

TEST(HistogramTest, createPositiveGrayscaleHistogramTest)
{
    Image testImage(Size(25, 25), ImageType::Grayscale);

    testImage.data.resize(testImage.size.m_width * testImage.size.m_height);
    std::fill(testImage.data.begin(), testImage.data.end(), 144);

    Histogram histogram;
    auto testHistogramVector = histogram.createGrayscaleHistogram(testImage);

    ASSERT_EQ(255, testHistogramVector.size());
    ASSERT_EQ(testImage.size.m_height * testImage.size.m_width, testHistogramVector[144]);
    ASSERT_EQ(0, testHistogramVector[23]);
}

TEST(HistogramTest, drawHistogramTest)
{
    const std::string testFileName = "drawHistogramTest.jpg";
    std::vector<int> testImageData(255, 0);
    testImageData[70] = 50;

    Histogram histogram;

    ASSERT_EQ(0, histogram.drawHistogram(testImageData, testFileName));

    std::ifstream ifile(testFileName);

    ASSERT_EQ(true, ifile.is_open());

    if (ifile.is_open())
    {
        ifile.close();
    }
}
