#include "tst_grayscaletest.h"
#include "tst_apptest.h"
#include "tst_averagetest.h"
#include "tst_mediantest.h"
#include "tst_transformationfactorytest.h"
#include "tst_blurtest.h"
#include "tst_histogramtest.h"

#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv); //inicjalizacja frameworka
    return RUN_ALL_TESTS();
}
