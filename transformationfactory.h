#ifndef TRANSFORMFACTORY_H
#define TRANSFORMFACTORY_H

#include <QImage>

#include <iostream>

#include "image.h"
#include "transformation.h"

#include "../googletest/googletest/include/gtest/gtest_prod.h"

class Blur;
class Grayscale;
class Median;
class Average;

/**
 * @brief The TransformationFactory class
 * Class that delivers specific object based on the parameter given by user
 */
class TransformationFactory
{
public:
    /**
     * @brief TransformationFactory -> default constructor
     */
    TransformationFactory();
    /**
     * @brief ~TransformationFactory -> virtual destructor
     */
    virtual ~TransformationFactory();
    /**
     * @brief createTransformation -> method that return pointer on the object, based on user input
     * @param transformationName -> name of transformation that user wants to perform
     * @param radius -> default parameter if user wants to blur image
     * @return pointer on the object
     */
    Transformation* createTransformation(const std::string transformationName, const int radius = 0);

private:
    /**
     * @brief m_possibleInputOperations -> map of possible operations that can be done
     */
    std::map<std::string, TransformOperation> m_possibleInputOperations; //zmiana nazwy
    FRIEND_TEST(TransformationFactoryTest, ConstructorGrayTest);
    FRIEND_TEST(TransformationFactoryTest, ConstructorMedianTest);
    FRIEND_TEST(TransformationFactoryTest, ConstructorAverageTest);
};

#endif // TRANSFORMFACTORY_H
