#include "app.h"

#include <QFile>
#include <QDebug>

App::~App()
{

}

App::App(int parametersNumber, char **argv) :
    m_parametersCounter(parametersNumber),
    m_parameters(argv),
    m_outputFileName(argv[parametersNumber - 1])
{
    m_programStatus = 0;

    if (m_parametersCounter < 3)
    {
        showInstruction();
    }

    m_possibleInputOperations["histogram"] = TransformOperation::Histogram;
    m_possibleInputOperations["gray"] = TransformOperation::Gray;
    m_possibleInputOperations["median"] = TransformOperation::Median;
    m_possibleInputOperations["average"] = TransformOperation::Average;

    m_neededParametersForOperation[TransformOperation::Histogram] = 0;
    m_neededParametersForOperation[TransformOperation::Gray] = 1;
    m_neededParametersForOperation[TransformOperation::Median] = 2;
    m_neededParametersForOperation[TransformOperation::Average] = 2;
}

int App::loadFile()
{
    if (m_programStatus == -1)
    {
        return m_programStatus;
    }

    if (!QFile::exists(m_parameters[1]))
    {
        std::cout << "File doesn't exist" << std::endl;
        m_programStatus = -1;
        return m_programStatus;
    }

    QImage inputImage(QString::fromStdString(m_parameters[1]));

    if (inputImage.isNull())
    {
        std::cout << "Something wrong with file" << std::endl;
        m_programStatus = -1;
        return m_programStatus;
    }

    Image image(Size(inputImage.width(), inputImage.height()), ImageType::Rgba);

    if (inputImage.isGrayscale())
    {
        image.type = ImageType::Grayscale;
    }

    std::copy(inputImage.bits(), inputImage.bits() + inputImage.byteCount(), std::back_inserter(image.data));

    m_programStatus = makeModification(image);

    return m_programStatus;
}

int App::saveTransformedFile(const Image &transformedImage)
{
    if (transformedImage.data.size() <= 0 || transformedImage.size.m_height <= 0 || transformedImage.size.m_width <= 0)
    {
        m_programStatus = -1;
        return m_programStatus;
    }

    QImage newImage(transformedImage.data.data(), transformedImage.size.m_width, transformedImage.size.m_height,
                transformedImage.size.m_width, QImage::Format_Grayscale8);
    const bool isSaveSuccess = newImage.save(QString::fromStdString(m_outputFileName), "JPG", 100);

    if (isSaveSuccess)
    {
        std::cout << "File saved!" << std::endl;
        m_programStatus = 0;
    }
    else
    {
        std::cout << "File not saved!" << std::endl;
        m_programStatus = -1;
    }

    return m_programStatus;
}

int App::makeModification(const Image &inputImage)
{
    Image modifiedImage = inputImage;
    TransformationFactory transformationFactory;

    const int PARAMETERS_INDEX = 2;
    std::string expectedModification = m_parameters[PARAMETERS_INDEX];

    TransformOperation operation = m_possibleInputOperations[expectedModification];

    checkInputParametersCount(operation);

    if (m_programStatus == -1)
    {
        return -1;
    }

    switch (operation)
    {
    case TransformOperation::Histogram:
    {
        Histogram histogram;
        m_programStatus = histogram.generateHistogram(modifiedImage);
    }
        break;

    case TransformOperation::Gray:
    {
        m_modification = transformationFactory.createTransformation(expectedModification);
        modifiedImage = m_modification->transform(modifiedImage);
        m_programStatus = saveTransformedFile(modifiedImage);
        delete m_modification;
    }
        break;

    case TransformOperation::Median:
    {
        const std::string blurRadius = m_parameters[PARAMETERS_INDEX + 1];
        m_modification = transformationFactory.createTransformation(expectedModification, std::stoi(blurRadius));
        modifiedImage = m_modification->transform(modifiedImage);
        delete m_modification;
        m_programStatus = saveTransformedFile(modifiedImage);
    }
        break;

    case TransformOperation::Average:
    {
        const std::string blurRadius = m_parameters[PARAMETERS_INDEX + 1];
        m_modification = transformationFactory.createTransformation(expectedModification, std::stoi(blurRadius));
        modifiedImage = m_modification->transform(modifiedImage);
        delete m_modification;
        m_programStatus = saveTransformedFile(modifiedImage);
    }
        break;

    default:
        m_programStatus = -1;
        break;
    }

    return m_programStatus;
}

void App::checkInputParametersCount(const TransformOperation operation)
{
    if ((m_parametersCounter - 3) != m_neededParametersForOperation[operation])
    {
        showInstruction();
    }
}

void App::showInstruction()
{
    std::cout << "\t\t\tInstruction\n------------------------------------------------------\n\
            1. Place image in project's catalog\n\
            2. Give image name\n\
            3. Give output file name\n\
            4. Give operation name:\n\
            \taverage\n\
            \tmedian\n\
            \thistogram\n\
            \tgray\n\n\
            In case You want to blur by median or average\n\
            be shure to precise radius just after blur command\n\
            (For example: input.jpg median 3 output.jpg)\n\n" << std::endl;

    m_programStatus = -1;
}
