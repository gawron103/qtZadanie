#include "app.h"

int main(int argc, char *argv[])
{
    App app(argc, argv);
    int exitStatus = app.loadFile();

    std::cout << "Program executed with status: " << exitStatus << std::endl;

    return exitStatus;
}
