#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "image.h"

/**
 * @brief The Transformation class
 * Interface for grayscale, histogram and blur class
 */
class Transformation
{
public:
    /**
     * @brief ~Transformation -> viertual destructor
     */
    virtual ~Transformation() {}
    /**
     * @brief transform -> method for modification
     * @param inputImage -> image given by user
     * @return
     */
    virtual Image transform(const Image &inputImage) = 0;
};

#endif // TRANSFORMATION_H
