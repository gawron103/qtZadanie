#include "transformationfactory.h"
#include "blur.h"
#include "grayscale.h"
#include "median.h"
#include "average.h"

TransformationFactory::TransformationFactory()
{
    m_possibleInputOperations["gray"] = TransformOperation::Gray;
    m_possibleInputOperations["median"] = TransformOperation::Median;
    m_possibleInputOperations["average"] = TransformOperation::Average;
}

TransformationFactory::~TransformationFactory()
{

}

Transformation *TransformationFactory::createTransformation(const std::string transformationName, const int radius)
{   
    TransformOperation operation = m_possibleInputOperations[transformationName];

    switch (operation)
    {
        case TransformOperation::Gray:
            return new Grayscale();
        break;

        case TransformOperation::Average:
            return new Average(radius);
        break;

        case TransformOperation::Median:
            return new Median(radius);
        break;

        default:
            return nullptr;
        break;
    }
}
