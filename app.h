#ifndef MENU_H
#define MENU_H

#include <QImage>

#include <iostream>
#include <map>
#include <stdexcept>

#include "image.h"
#include "transformationfactory.h"
#include "histogram.h"

#include "../googletest/googletest/include/gtest/gtest_prod.h"

/**
 * @brief The App class
 * The task of this class is to load the image, select the modifications according to the user
 * guidelines and save modified image.
 */

class App
{
public:
    /**
     * @brief App -> Default constructor.
     */
    App();
    /**
     * @brief App -> Constructor
     * @param parametersNumber -> number of informations given by user
     * @param argv -> pointer of informations given by user
     */
    App(int parametersNumber, char **argv);
    ~App();

    /**
     * @brief loadFile - > public method that load's specified by user image
     * and creates struct based on that image data
     */
    int loadFile();

//private:
protected:
    /**
     * @brief m_programStatus -> output status of program
     */
    int m_programStatus;
    /**
     * @brief m_parametersCounter -> number of informations given by user
     */
    int m_parametersCounter;
    /**
     * @brief m_parameters -> pointer of informations given by user
     */
    char **m_parameters;
    /**
     * @brief m_modification - pointer to the object that will perform transformation
     */
    Transformation *m_modification = nullptr;
    /**
     * @brief m_outputFileName -> name for output file
     */
    std::string m_outputFileName;
    /**
     * brief m_possibleInputOperations -> available transform operations
     */
    std::map<std::string, TransformOperation> m_possibleInputOperations;
    /**
     * @brief m_neededParametersForOperation -> number of required parameters for
     * a particular modification
     */
    std::map<TransformOperation, int> m_neededParametersForOperation;
    /**
     * @brief saveTransformedFile -> private method that can save modified image as new file
     * @param transformedImage -> modified image
     */
    int saveTransformedFile(const Image& transformedImage);
    /**
     * @brief makeModification -> private method that choose proper modification object and performs
     * it on given image
     * @param inputImage -> image
     */
    int makeModification(const Image &inputImage);
    /**
     * @brief checkInputParametersCount -> private method that checks if user gave enought details
     * about choosen modification
     * @param operation - > transform operation chosen by user
     */
    void checkInputParametersCount(const TransformOperation operation);
    /**
     * @brief showInstruction -> private method that prints instruction of program if user
     * gave not enough information for choosen transformation
     */
    void showInstruction();

    FRIEND_TEST(AppTest, GoodConstructorTest);
    FRIEND_TEST(AppTest, BadConstructorTest);
    FRIEND_TEST(AppTest, SaveSuccessTransformedTest);
    FRIEND_TEST(AppTest, SaveFailureTransformedTest);
    FRIEND_TEST(AppTest, checkWrongInputParametersCountTest);
    FRIEND_TEST(AppTest, checkGoodInputParametersCountTest);
    FRIEND_TEST(AppTest, makePositiveGrayscaleModificationTest);
    FRIEND_TEST(AppTest, makePositiveAverageModificationTest);
    FRIEND_TEST(AppTest, makePositiveMedianModificationTest);
    FRIEND_TEST(AppTest, makePositiveHistogramModificationTest);
    FRIEND_TEST(AppTest, makeNegativeModificationTest);
};

#endif // MENU_H
